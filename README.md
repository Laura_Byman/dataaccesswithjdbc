# Java Assignment 2: Data access with JDBC

## Table of contents

- Background
- Installation
- Contributors


### Background
This project contains two separate exercises: 
1. SQL scripts and 
2. a Spring Boot application which interacts with a PostreSQL database.

#### 1.  SQL Scripts (Superheroes)

The superheroes-folder contains nine different SQL-script files.
Scripts include database and table and relation creation, with data insertion, manipulation and deletion.

#### 2. Java Spring Boot Application with DB

The Chinook-folder contains a Spring Boot application which interacts with a PostreSQL database.
DB models an iTunes-like database of customers purchasing songs. 
The application has a Customer Repository which inherits the CRUD-parents methods to access, manipulate and delete data from DB.
In the repository there is also a few unique methods implemented, e.g. one for finding the country where most of the customers live.

### Installation

1. Clone this repository with HTTPS
2. Open project in IDE
3. Create Chinook db in PgAdmin and connect the Chinook-projest to the db.

### Contributors

This exercise was created by Noroff School of Technology and this solution was implemented by Laura Byman and Lotta Lampola.






