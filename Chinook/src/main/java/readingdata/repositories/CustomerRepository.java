package readingdata.repositories;

import readingdata.models.Customer;
import readingdata.models.CustomerCountry;
import readingdata.models.CustomerGenre;
import readingdata.models.HighestSpendingCustomer;

import java.util.ArrayList;
import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer>{
    void test();

    Customer findByName(String name);

    List<Customer> readSubSetOfCustomers(int offset, int limit);

    CustomerCountry readCountryWithMostCustomers();

    HighestSpendingCustomer readHighestSpendingCustomer();

    ArrayList<CustomerGenre> mostPopularGenre(Customer customer3);
}
