package readingdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadingdatawithjdbcApplication {


	public static void main(String[] args) {

		SpringApplication.run(ReadingdatawithjdbcApplication.class, args);

	}

}
