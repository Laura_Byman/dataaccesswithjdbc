package readingdata.runner;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import readingdata.models.Customer;
import readingdata.models.CustomerCountry;
import readingdata.models.CustomerGenre;
import readingdata.models.HighestSpendingCustomer;
import readingdata.repositories.CustomerRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppRunner implements ApplicationRunner {

    private final CustomerRepository crepo;

    public AppRunner(CustomerRepository crepo) {
        this.crepo = crepo;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        crepo.test();

        //Test read all
        List<Customer> customers = crepo.findAll();
        customers.forEach(System.out::println);
        System.out.println(customers.size());

        //Find by id
        Customer customer = (Customer) crepo.findById(5);
        System.out.println(customer);
        //Find by name
        Customer customer1 = crepo.findByName("Helena");
        System.out.println(customer1);
        //limit and offset test
        System.out.println("Limit and offset:");
        List <Customer> limited = crepo.readSubSetOfCustomers(3,10);
        limited.forEach(System.out::println);
        //Insert
        Customer customer2 = new Customer(0,"Kathryn", "Gomez", "Brazil",
                "01007-010", "+55 (11) 3033-5448",  "cathryn@uol.com.br" );
        System.out.println(customer2);
        crepo.insert(customer2);
        List<Customer> customers2 = crepo.findAll();
        //Update
        Customer updatedCustomer = new Customer(customer1.id(), customer1.firstName(), customer1.lastName(),
                "Austria", "01310-500", customer1.phoneNumber(), customer1.email());
        crepo.update(updatedCustomer);
        System.out.println(crepo.findByName("Helena"));


        //read country with most customers
        CustomerCountry country = crepo.readCountryWithMostCustomers();
        System.out.println("Country: "+country);
        //read highest spending customer
        HighestSpendingCustomer highest = crepo.readHighestSpendingCustomer();
        System.out.println("Highest spending customer: "+highest);
        Customer customer3 = (Customer) crepo.findById(1);
        ArrayList<CustomerGenre> mostPopularGenre = crepo.mostPopularGenre(customer3);
        System.out.println("Most popular genre:");
        mostPopularGenre.stream().forEach(genre -> System.out.println(genre));

    }

}

