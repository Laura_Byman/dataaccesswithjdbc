package readingdata.models;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import readingdata.repositories.CustomerRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private String url;

    private String username;

    private String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * <p>Get all customers from database</p>
     * @return a list of all Customer records in database, return null in case of exception
     *
     */
    @Override
    public List findAll() {
        String sql = "SELECT * FROM Customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer Customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(Customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return customers;
    }

    /**
     * <p>Get customer information corresponding given customer id from database</p>
     * @param id  customer id
     * @return Customer record with given id, return null in case of exception
     */
    @Override
    public Customer findById(Integer id) {
        String sql = "SELECT * FROM Customer WHERE customer_id=?";
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, (Integer) id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return customer;
    }

    /**
     * <p>Get customer or customers information corresponding given name from database, name can be lastname or firstname</p>
     * @param name String type parameter, first name or last name of customer
     * @return customer(s) with given name from database, return null in case of exception
     */
    public Customer findByName(String name){
        String sql = "SELECT * FROM Customer WHERE last_name=? OR first_name=?";
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2,name);
            // Execute statement
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return customer;
    }


    /**
     * <p>get information of selected group of customers from database, get all customers between given id values.</p>
     * @param offset int type id of the first customer
     * @param limit int type id of the last customer
     * @return List of Customer records between given id values, return null in case of exception
     */
    public List <Customer> readSubSetOfCustomers(int offset, int limit){
        String sql = "SELECT * FROM Customer order by customer_id offset ? limit ?";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, offset);
            statement.setInt(2,limit);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer Customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(Customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return customers;
    }

    /**
     * <p>Insert a new customer information to database</p>
     * @param customer Customer record to be inserted
     * @return 0 if insert succeeds, -1 if not succesful
     */
    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO Customer(first_name, last_name, country, postal_code, phone, email) " +
                "VALUES (?,?,?,?,?,?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * <p>Update customers information</p>
     * @param customer Customer record to be updated
     * @return 0 if insert succeeds, -1 if not succesful
     */
    @Override
    public int update(Customer customer) {
        String sql = "UPDATE Customer SET first_name=?, last_name=?, country=?, postal_code=?, " +
                "phone=?, email=? WHERE customer_id=?";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.id());
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int delete(Customer customer) {
        return 0;
    }

    @Override
    public int deleteById(Integer id) {
        return 0;
    }

    /**
     * <p>Function returns name of country with most customers</p>
     * @return Record customerCountry with name and customer count of country with most customers. Return null in case of exception.
     */
    public CustomerCountry readCountryWithMostCustomers(){
        CustomerCountry country = null;
        String sql = "SELECT country,count(customer_id) as customercount FROM customer group by country order by customercount desc limit 1";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                country = new CustomerCountry(
                        result.getString("country"),
                        result.getInt("customercount")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return country;
    }

    /**
     * <p>Returns information of customer who is the highest spender (the largest total in invoice table)</p>
     * @return Record HighestSpendingCustomer with customer info and total sum spent. Return null in case of exception.
     */
    public HighestSpendingCustomer readHighestSpendingCustomer(){
        String sql = "select inv.customer_id, sum(total), cu.first_name, cu.last_name, cu.country, cu.postal_code, cu.phone, cu.email from invoice as inv\n" +
                "join customer as cu ON cu.customer_id = inv.customer_id\n" +
                "group by inv.customer_id, cu.last_name, cu.first_name, cu.country, cu.postal_code, cu.phone, cu.email\n" +
                "order by sum(total) desc\n" +
                "limit 1\n";
        HighestSpendingCustomer customer = null;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer = new HighestSpendingCustomer(
                        result.getInt("customer_id"),
                        result.getDouble("sum"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;

    }

    /**
     * <p>Returns most popular genre of given customer (genre that corresponds the most tracks customer has bought</p>
     * @param customer Customer whose info to search
     * @return List of strings, most popular genre or genres. In case of ties return all genres that are equally popular.
     * Return null in case of exception.
     */
    public ArrayList<CustomerGenre> mostPopularGenre(Customer customer){
        String sql = "SELECT customer.customer_id, genre.name, count(genre.name) FROM customer " +
                "INNER JOIN invoice ON customer.customer_id = invoice.customer_id " +
                "INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id " +
                "INNER JOIN track ON invoice_line.track_id  = track.track_id " +
                "INNER JOIN genre ON track.genre_id = genre.genre_id " +
                "WHERE customer.customer_id=? "+
                "GROUP BY  customer.customer_id, genre.name ORDER BY  count DESC " +
                "FETCH FIRST 1 ROWS WITH TIES ";
        ArrayList<CustomerGenre> customergenres = new ArrayList<>();
        int count =0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.id());
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                CustomerGenre cgenre = new CustomerGenre(
                        result.getInt("customer_id"),
                        result.getString("name"),
                        result.getInt("count")
                );
                customergenres.add(cgenre);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return customergenres;

    }

    /**
     * function to test connection to Postgress
     * prints "Connected to Postgres..." if connection succeeds
     */
    @Override
    public void test() {

        try(Connection conn = DriverManager.getConnection(url, username,password);) {
            System.out.println("CONNECTED TO Postgres...");
        } catch (SQLException e) {
            e.printStackTrace();
        }



    }
}
