package readingdata.models;

/**
 * Record of Customer Genre
 * @param customer_id id value of customer
 * @param genre genre
 * @param count count of tracks of given genre for given customer
 */



public record CustomerGenre(int customer_id,
                            String genre,
                            int count) {
}
