package readingdata.models;

/**
 * Record of Customer
 *
 * @param id id value of customer
 * @param firstName first name of customer
 * @param lastName last name of customer
 * @param country country of customer
 * @param postalCode postalCode of customer
 * @param phoneNumber phone number of customer
 * @param email email of customer

 */
public record Customer(int id,
                       String firstName,
                       String lastName,
                       String country,
                       String postalCode,
                       String phoneNumber,
                       String email) {

}
