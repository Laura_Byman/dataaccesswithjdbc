package readingdata.models;

/**
 * Record of CustomerCountry
 * @param name name of country
 * @param customerCount count of customers from given country
 */
public record CustomerCountry(String name,
                              int customerCount) {
}
