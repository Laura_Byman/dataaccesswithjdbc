package readingdata.models;


/**
 * HighestSpendingCustomer
 *
 * @param customer_id id value of customer
 * @param totalSpent double type, total value customer has spent
 * @param firstName first name of customer
 * @param lastName last name of customer
 * @param country country of customer
 * @param postalCode postalCode of customer
 * @param phoneNumber phone number of customer
 * @param email email of customer

 */
public record HighestSpendingCustomer(int customer_id,
                                      double totalSpent,
                                      String firstName,
                                      String lastName,
                                      String country,
                                      String postalCode,
                                      String phoneNumber,
                                      String email) {
}
