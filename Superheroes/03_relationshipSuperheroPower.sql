DROP TABLE IF EXISTS hero_heropower;

CREATE TABLE hero_heropower (
    superhero_id int REFERENCES superhero,
    heropower_id int REFERENCES heropower,
    PRIMARY KEY (superhero_id, heropower_id)
);