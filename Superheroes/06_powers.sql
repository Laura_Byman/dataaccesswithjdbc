INSERT INTO heropower VALUES (DEFAULT, 'flying', 'ability to fly');

INSERT INTO heropower VALUES (DEFAULT, 'psionics', 'energy manipulation and telekinesis');

INSERT INTO heropower VALUES (DEFAULT, 'fighting skills', 'Master fighting skills');

INSERT INTO heropower VALUES (DEFAULT, 'strength', 'inhuman strength');

INSERT INTO hero_heropower VALUES (1,1);

INSERT INTO hero_heropower VALUES (1,3);

INSERT INTO hero_heropower VALUES (2,1);

INSERT INTO hero_heropower VALUES (2,4);

INSERT INTO hero_heropower VALUES (3,2);