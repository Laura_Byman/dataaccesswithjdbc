ALTER TABLE assistant
ADD COLUMN superhero_id integer,
ADD CONSTRAINT FK_superhero
FOREIGN KEY (superhero_id) REFERENCES superhero(superhero_id);
