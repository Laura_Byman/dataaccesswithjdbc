-- Database: superheroesDb

-- DROP DATABASE IF EXISTS superheroesDb;

CREATE DATABASE superheroesDb
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_Finland.1252'
    LC_CTYPE = 'English_Finland.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    