DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero (
    superhero_id serial PRIMARY KEY,
    superhero_name varchar(50) NOT NULL,
    superhero_alias varchar(50) NOT NULL,
    superhero_origin varchar(50) NOT NULL
);

DROP TABLE IF EXISTS assistant;

CREATE TABLE assistant (
    assistant_id serial PRIMARY KEY,
    assistant_name varchar(50) NOT NULL
);

DROP TABLE IF EXISTS heroPower;

CREATE TABLE heroPower (
    heroPower_id serial PRIMARY KEY,
    heroPower_name varchar(50) NOT NULL,
    heroPower_description varchar(1000) NOT NULL
);

